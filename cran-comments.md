## Resubmission

This is a resubmission. In this version I have:

-   Updated bct() and CalcBoundaryConvex() methods functionality to
    increase run time.

## Test environments

-   local Windows 10 install, R 4.1.1
-   win-builder (devel)
-   rhub (macOS 10.11 El Capitan, Debian Linux R-devel GCC)

## R CMD check results

There were no ERRORs or WARNINGs.

There was 1 NOTE:

-   NOTE information on .o files for x64 is not available

Couldn’t resolve this note.

## Downstream dependencies

There are currently no downstream dependencies for this package.
